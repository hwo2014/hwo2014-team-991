﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CSharp_GUI
{
    public partial class Form1 : Form
    {

        BotControl bot;

        public Form1()
        {
            InitializeComponent();

            bot = new BotControl();


            chart1.Titles.Add("Throttle");
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            bot.Stop();

            base.OnFormClosing(e);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            chart3.Series[0].Points.Add(bot.speed);
            chart3.Series[1].Points.Add(bot.acceleration);

            float slipAngle = (bot.lastOwnCarPosition != null) ? bot.lastOwnCarPosition.angle : 0.0f;
            chart2.Series[0].Points.Add(slipAngle);

            this.chart1.Series[0].Points.Add(bot.lastThrottleValue);

            textBox1.Text = bot.maxSlip.ToString();
            textBox2.Text = bot.Gmax.ToString();
            textBox3.Text = bot.pid.Setpoint.ToString();
            textBox4.Text = bot.pidslip.Setpoint.ToString();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
