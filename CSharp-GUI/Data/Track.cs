﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CopernicusBot.Data
{
    public class Position
    {
        public float x { get; set; }
        public float y { get; set; }
    }

    public class StartingPoint
    {
        public Position position { get; set; }
        public float angle { get; set; }
    }

    public class RaceSession
    {
        public int laps { get; set; }

        public float maxLapTimeMs { get; set; }

        public bool quickRace { get; set; }
    }

    public class Track
    {

        public string id { get; set; }
        public string name { get; set; }

        public List<TrackPiece> pieces = new List<TrackPiece>();

        public List<Lane> lanes = new List<Lane>();

        public StartingPoint startingPoint = new StartingPoint();

        public List<Car> cars = new List<Car>();

        public RaceSession raceSession = new RaceSession();
        
    }
}
