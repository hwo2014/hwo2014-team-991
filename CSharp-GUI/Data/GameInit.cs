﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopernicusBot.Data
{
    
    public class GameInit
    {
        
        public class RaceSession
        {
            public int laps;
            public int maxLapTimeMs;
            public bool quickRace;
        }

        
        public class Race
        {
            public Track track;
            public Car[] cars;
            public RaceSession raceSession;
        }

        public Race race;
    }
}
