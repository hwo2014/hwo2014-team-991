﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopernicusBot.Data
{

    public class CarId
    {
        public string name { get; set; }
        public string color { get; set; }
        public string key { get; set; }
    }

    
    public class Dimensions
    {
        public float length { get; set; }
        public float width { get; set; }
        public float guideFlagPosition { get; set; }
    }

    
    public class Car
    {
        public CarId id { get; set; }
        public Dimensions dimensions { get; set; }
    }
}
