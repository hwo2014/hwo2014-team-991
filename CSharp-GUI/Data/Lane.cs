﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CopernicusBot.Data
{
    public class Lane
    {
        public float distanceFromCenter { get; set; }

        public int index { get; set; }
    }
}
