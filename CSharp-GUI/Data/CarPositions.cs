﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopernicusBot.Data
{
    
    public class CarPositions
    {
        public CarPosition[] positions;
    }

    
    public class CarPosition
    {
        
        public class PiecePosition
        {
            
            public class Lane
            {
                public int startLaneIndex;
                public int endLaneIndex;
            }

            public int pieceIndex;
            public float inPieceDistance;
            public int lap;
            public Lane lane;
        }

        public CarId id;
        public float angle;
        public PiecePosition piecePosition;
    }
}
