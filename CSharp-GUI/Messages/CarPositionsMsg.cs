﻿using CopernicusBot.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopernicusBot.Messages
{
    public class CarPositionsMsg : Msg<CarPosition[]>
    {
        public string gameId;
        public int gameTick;
    }
}
