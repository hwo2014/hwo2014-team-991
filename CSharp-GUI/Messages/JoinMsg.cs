﻿using CopernicusBot.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopernicusBot.Messages
{
    public class JoinMsg : Msg<CarId>
    {
        public JoinMsg(string name, string key)
        {
            msgType = "join";
            data = new CarId();
            data.name = name;
            data.color = "red";
            data.key = key;
        }

        public JoinMsg(string name, string key, string color)
        {
            msgType = "join";
            data = new CarId();
            data.name = name;
            data.color = "red";
            data.key = key;
        }
    }
}
