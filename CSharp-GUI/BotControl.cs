﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_GUI
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Net.Sockets;
	using System.Threading;
	using Newtonsoft.Json;
	using CopernicusBot;
	using CopernicusBot.Messages;
    using CopernicusBot.Data;
    using CopernicusBot.AI;

	public class BotControl
	{
		public static string host;
		public static int port;
		public static string botName;
		public static string botKey;

		public static void Main(string[] args)
		{
			BotControl.host = args[0];
			BotControl.port = int.Parse(args[1]);
			BotControl.botName = args[2];
			BotControl.botKey = args[3];

			Console.WriteLine(string.Format("Connecting to {0} : {1} as {2} / {3}", host, port, botName, botKey));

			new BotControl();
		}

		private StreamWriter writer;

		public int AITimeout = 50;
		public int AICheckInterval = 1;

		public List<Msg> Messages = new List<Msg>();
		public Msg NextMsg = null;

		
		private Thread _readThread;
		private Thread _aiThread;

		public bool Running { get { return _running; } }

		private bool _botFinished;        
		private bool _running;
		private bool msgSent;

		public int ReceivedCount { get; private set; }
		public int SentCount { get; private set; }

		public string gameId;
		public int lastGameTick;
		public CarId ownCarId;
		public CarId[] carIds;
		public CarPosition lastOwnCarPosition;
		public CarPosition[] lastCarPositions;
		public List<CarPositions> carPositions = new List<CarPositions>();
		public LapFinished lastOwnLap;
		public GameInit gameInit;
		public GameEnd gameEnd;
		public GameEnd.Result ownResult;
		public GameEnd.BestLap ownBestLap;

		public TrackGraph trackGraph;

		public BotControl()
		{
			gameId = null;
			lastGameTick = 0;
			ownCarId = null;
			carIds = null;
			lastOwnCarPosition = null;
			lastCarPositions = null;
			carPositions.Clear();
			lastOwnLap = null;
			gameInit = null;
			gameEnd = null;
			ownResult = null;
			ownBestLap = null;

			Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            _running = true;
			_botFinished = false;
			_readThread = new Thread(Read);
			_readThread.Start();
			_aiThread = new Thread(Update);
			_aiThread.Start();
		}

		public void Stop()
		{
			if (!_running)
			{
				return;
			}
			_botFinished = true;
			_running = false;

			if (_readThread != null)
			{
				_readThread.Abort();
			}

			if (_aiThread != null)
			{
				_aiThread.Abort();
			}
		}

		void Read()
		{
			using (TcpClient client = new TcpClient(host, port))
			{
				NetworkStream stream = client.GetStream();
				StreamReader reader = new StreamReader(stream);
				StreamWriter writer = new StreamWriter(stream);
				writer.AutoFlush = true;

				ReceivedCount = 0;
				SentCount = 0;
				Messages.Clear();

				Read(reader, writer);
			}
		}

		void Read(StreamReader reader, StreamWriter writer)
		{
			this.writer = writer;

			SendMsg(new JoinMsg(botName, botKey));

			string line;
			while ((line = reader.ReadLine()) != null)
			{
				Msg msg = JsonConvert.DeserializeObject<Msg<Object>>(line);
				Msg<CarId> carMsg = null;
				Msg<LapFinished> lapMsg = null;
				Msg<GameInit> initMsg = null;
				Msg<GameEnd> endMsg = null;

				msgSent = false;

				switch (msg.msgType)
				{
					case "dnf":
						carMsg = JsonConvert.DeserializeObject<Msg<CarId>>(line);
						Console.WriteLine(String.Format("Disqualified: {0} ({1})", carMsg.data.name, carMsg.data.color));
						break;

					case "crash":
						carMsg = JsonConvert.DeserializeObject<Msg<CarId>>(line);
						Console.WriteLine(String.Format("Crashed: {0} ({1}) angle: {2}", carMsg.data.name, carMsg.data.color, lastOwnCarPosition.angle));
						maxSlip -= 2f;

                        Gmax = 0.975f * speed * speed / gameInit.race.track.pieces[lastOwnCarPosition.piecePosition.pieceIndex].radius;

                        Console.WriteLine(String.Format("{0} {1}", speed, pidslip.Setpoint * 0.975f));

                        pidslip.Setpoint = Math.Min(speed, pidslip.Setpoint * 0.975f);
                        

						break;

					case "spawn":
						carMsg = JsonConvert.DeserializeObject<Msg<CarId>>(line);
						Console.WriteLine(String.Format("Spawned: {0} ({1})", carMsg.data.name, carMsg.data.color));
						break;

					case "carPositions":
						CarPositionsMsg carPositionsMsg = JsonConvert.DeserializeObject<CarPositionsMsg>(line);
						msg = carPositionsMsg;
						gameId = carPositionsMsg.gameId;
						carPositions.Add(new CarPositions() { positions = carPositionsMsg.data });
						lastCarPositions = carPositionsMsg.data;
						lastGameTick = carPositionsMsg.gameTick;
						if (ownCarId != null)
						{
							foreach (var carPosition in lastCarPositions)
							{
								if (carPosition.id.color == ownCarId.color)
									lastOwnCarPosition = carPosition;
							}
						}
						SendNextMsg();
						break;

					case "lapFinished":
						lapMsg = JsonConvert.DeserializeObject<Msg<LapFinished>>(line);
						Console.WriteLine(String.Format("Lap {0}: {1:F2}s ({2:F2}s) {3} ({4}) [#{5} overall, #{6} lap])",
												lapMsg.data.lapTime.lap + 1,
												lapMsg.data.raceTime.millis / 1000f,
												lapMsg.data.lapTime.millis / 1000f,
												lapMsg.data.car.name,
												lapMsg.data.car.color,
												lapMsg.data.ranking.overall,
												lapMsg.data.ranking.fastestLap));
						if (ownCarId != null && lapMsg.data.car.color == ownCarId.color)
						{
							lastOwnLap = lapMsg.data;
						}
						computedFirstSwitch = false;
						break;

					case "join":
						carMsg = JsonConvert.DeserializeObject<Msg<CarId>>(line);
						Console.WriteLine(string.Format("Joined: {0} - {1}", carMsg.data.name, carMsg.data.color));
						break;

					case "finish":
						carMsg = JsonConvert.DeserializeObject<Msg<CarId>>(line);
						Console.WriteLine(String.Format("Finished: {0} ({1})", carMsg.data.name, carMsg.data.color));
						break;

					case "yourCar":
						carMsg = JsonConvert.DeserializeObject<Msg<CarId>>(line);
						ownCarId = carMsg.data;
						Console.WriteLine(string.Format("Car Info: {0} - {1}", carMsg.data.name, carMsg.data.color));
						break;

					case "gameInit":

						Console.WriteLine(line);

						initMsg = JsonConvert.DeserializeObject<Msg<GameInit>>(line);
						gameInit = initMsg.data;

						trackGraph = new TrackGraph(gameInit.race.track);

						Console.WriteLine(string.Format("Race init: {0} with {1} cars - {2} laps", gameInit.race.track.id, gameInit.race.cars.Length, gameInit.race.raceSession.laps));

						carIds = new CarId[gameInit.race.cars.Length];
						for (int id = 0; id < carIds.Length; ++id)
						{
							carIds[id] = gameInit.race.cars[id].id;
						}

						break;

					case "gameEnd":

						endMsg = JsonConvert.DeserializeObject<Msg<GameEnd>>(line);
						gameEnd = endMsg.data;

						Console.WriteLine("Race ended");
						Console.WriteLine("Rankings:");
						for (int i = 0; i < gameEnd.results.Length; ++i)
						{
							GameEnd.Result result = gameEnd.results[i];
							if (result.car.color == ownCarId.color)
							{
								ownResult = result;
							}

							Console.WriteLine(string.Format("#{0} - {1:F2}s {2} ({3})", i + 1, result.result.millis, result.car.name, result.car.color));
						}
						Console.WriteLine("\nBest Laps:");
						for (int i = 0; i < gameEnd.bestLaps.Length; ++i)
						{
							GameEnd.BestLap bestLap = gameEnd.bestLaps[i];
							if (bestLap.car.color == ownCarId.color)
							{
								ownBestLap = bestLap;
							}

							Console.WriteLine(string.Format("#{0} - {1:F2}s lap {2} - {3} ({4})", i + 1, bestLap.result.millis, bestLap.result.lap, bestLap.car.name, bestLap.car.color));
						}

						break;

					case "gameStart":
						Console.WriteLine("Race starts");
						break;

					case "tournamentEnd":
						Console.WriteLine("Tournament Ended");
						break;

					default:
						Console.WriteLine(String.Format("Unknown message type: {0} {1}", msg.msgType, msg.ToJson(false, false)));
						break;
				}

				if (!msgSent)
				{
					SendMsg(new PingMsg());
				}
				msg.SetMsgDirection(Msg.EMsgDirection.Received);
				Messages.Add(msg);
				++ReceivedCount;
			}
		}

		void ComputeLaneSwitch(CarPosition.PiecePosition piecePos)
		{
			int laneIndex = piecePos.lane.endLaneIndex;

			int nextTurn = trackGraph.PickLaneSwitch(piecePos.pieceIndex + 1, laneIndex);

			if (nextTurn == -1)
			{
				Console.WriteLine("Left Turn");
				NextMsg = new SwitchLaneMsg("Left");
				isSwitching = true;
			}
			else if (nextTurn == 1)
			{
				Console.WriteLine("Right Turn");
				NextMsg = new SwitchLaneMsg("Right");
				isSwitching = true;
			}
			else
			{
				Console.WriteLine("Straight through");
				NextMsg = new ThrottleMsg(0.6f);
				isSwitching = false;
			}
		}

		public float maxSlip = 60.0f;
		bool computedFirstSwitch = false;
		bool isSwitching = false;
        public float lastThrottleValue = 0.0f;
        public float lastInPieceDist = 0.0f;
        public float acceleration = 0.0f;
        public float speed = 0.0f;
        public const float maxAngleSpeed = 6.25f;
        public AbstergoPID.PID pid = new AbstergoPID.PID(0.0f, 1.0f, 10.0f, 0.2f, 0.01f, 1.0f, AbstergoPID.PID.MovementDirection.Direct);
        public AbstergoPID.PID pidslip = new AbstergoPID.PID(0.0f, 1.0f, maxAngleSpeed, 0.1f, 0.005f, 1.0f, AbstergoPID.PID.MovementDirection.Direct);
        public float Gmax = -1.0f;

		void Update()
		{
			bool hasComputedNextSwitch = false;
            pid.SetOutputLimits(0.0f, 1.0f);
            pidslip.SetOutputLimits(0.0f, 1.0f);

			while (!_botFinished)
			{
				if (lastOwnCarPosition != null && (NextMsg == null || NextMsg.MsgDirection() == Msg.EMsgDirection.Sent))
				{
                    //TODO: track track piece ids as well for multiple track piece distances
                    // PrevCar--|----|-----|--Car
                    float lastSpeed = speed;

                    if (lastOwnCarPosition.piecePosition.inPieceDistance < lastInPieceDist)
                    {
                        int pieceIndex = lastOwnCarPosition.piecePosition.pieceIndex - 1;
                        if (pieceIndex < 0)
                            pieceIndex = gameInit.race.track.pieces.Count - 1;

                        speed = lastOwnCarPosition.piecePosition.inPieceDistance + trackGraph.CalculateLength(gameInit.race.track.pieces[pieceIndex], gameInit.race.track.lanes[lastOwnCarPosition.piecePosition.lane.startLaneIndex].distanceFromCenter) - lastInPieceDist;
                    }
                    else
                    {
                        speed = lastOwnCarPosition.piecePosition.inPieceDistance - lastInPieceDist;
                    }

                    acceleration = Math.Abs(speed - lastSpeed);

                    lastInPieceDist = lastOwnCarPosition.piecePosition.inPieceDistance;

					if ((!computedFirstSwitch) ||
						(!hasComputedNextSwitch && gameInit.race.track.pieces[lastOwnCarPosition.piecePosition.pieceIndex].@switch))
					{
						computedFirstSwitch = true;
						hasComputedNextSwitch = true;
						ComputeLaneSwitch(lastOwnCarPosition.piecePosition);
					}
					else if (!gameInit.race.track.pieces[lastOwnCarPosition.piecePosition.pieceIndex].@switch)
					{
						hasComputedNextSwitch = false;

                        EstimateThrottle();
					}
					else
					{
                        EstimateThrottle();
					}
				}
			}
		}

        void EstimateThrottle()
        {
            float maxDist = 200.0f;
            float distance = 0.0f;
            for (int i = lastOwnCarPosition.piecePosition.pieceIndex; i < gameInit.race.track.pieces.Count; ++i)
            {
                if (distance >= maxDist || (gameInit.race.track.pieces[i].@switch && isSwitching) || gameInit.race.track.pieces[i].angle != 0.0f)
                    break;
                distance += trackGraph.CalculateLength(gameInit.race.track.pieces[i], 0.0f);
            }
            distance -= lastOwnCarPosition.piecePosition.inPieceDistance;
            distance = Math.Min(0.0f, distance);

            float currentSlip = Math.Abs(lastOwnCarPosition.angle);

            if (distance == 0.0f)
            {
                if (Gmax > 0.0f && ((currentSlip / maxSlip) < 0.12f) && maxAngleSpeed > pidslip.Setpoint)
                {
                    Gmax = speed * speed / gameInit.race.track.pieces[lastOwnCarPosition.piecePosition.pieceIndex].radius;
                    pidslip.Setpoint += pidslip.Setpoint * 0.001f;
                }

                distance = 1.0f - (0.25f * currentSlip / maxSlip);
            }
            else if (distance < maxDist)
                distance = 1.0f - distance / 200.0f;
            else
            {
                distance = 0.0f;
            }
            distance = Math.Min(1.0f, distance);

            if (currentSlip > maxSlip)
            {
                Gmax = speed * speed / gameInit.race.track.pieces[lastOwnCarPosition.piecePosition.pieceIndex].radius;
            }

            if (Gmax > 0.0f && gameInit.race.track.pieces[lastOwnCarPosition.piecePosition.pieceIndex].radius != 0.0f)
            {
                pidslip.Setpoint = Math.Min(pidslip.Setpoint, (float)Math.Sqrt(gameInit.race.track.pieces[lastOwnCarPosition.piecePosition.pieceIndex].radius) * Gmax);
            }

            lastThrottleValue = Math.Min(Math.Max(0.0f, currentSlip), 1.0f);
            //NextMsg = new ThrottleMsg(lastThrottleValue);

            lastThrottleValue = Math.Min(Math.Max(0.0f, currentSlip), 1.0f);
            //NextMsg = new ThrottleMsg(lastThrottleValue);

            pid.Input = speed;
            pid.Compute();
            //Console.WriteLine(pid.Output);
            pidslip.Input = speed;
            pidslip.Compute();

            float speedPri = 1.0f - distance;
            float anglePri = distance;

            float value = pid.Output * speedPri + pidslip.Output * anglePri / (speedPri + anglePri);
            value = Math.Min(Math.Max(0.0f, value), 1.0f);
            lastThrottleValue = value;
            //NextMsg = new ThrottleMsg(lastThrottleValue);
            NextMsg = new ThrottleMsg(value);

            //Console.WriteLine("Throttle: {0}, Speed Pri: {1}, Angle Pri {2}, PIDSpeed: {3}, PIDAngle {4}", value, speedPri, anglePri, pid.Output, pidslip.Output);
        }


		void SendNextMsg()
		{
			Msg nextMsg = null;
			int timeout = AITimeout;
			while (timeout > 0)
			{
				nextMsg = NextMsg;
				if (nextMsg != null)
					break;
				timeout -= AICheckInterval;
				Thread.Sleep(AICheckInterval);
			}
			if (nextMsg != null)
				SendMsg(nextMsg);
		}

		void SendMsg(Msg msg)
		{
			msg.SetMsgDirection(Msg.EMsgDirection.Sent);
			writer.WriteLine(msg.ToJson(false, true));
		}
	}

}
