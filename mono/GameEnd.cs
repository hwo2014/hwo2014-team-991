﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopernicusBot
{
    public class GameEnd
    {
        public class Result
        {
            public CarId car { get; set; }
            public RaceTime result { get; set; }
        }

        public class BestLap
        {
            public CarId car { get; set; }
            public LapTime result { get; set; }
        }

        public Result[] results;
        public BestLap[] bestLaps;
    }
}
