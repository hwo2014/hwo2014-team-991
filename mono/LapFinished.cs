﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopernicusBot
{
    public class LapFinished
    {
        public CarId car { get; set; }
        public LapTime lapTime { get; set; }
        public RaceTime raceTime { get; set; }
        public Ranking ranking { get; set; }
    }
}
