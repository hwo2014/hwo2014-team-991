﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopernicusBot.Messages
{
    public class SwitchLaneMsg : Msg<string>
    {
        public SwitchLaneMsg(string direction)
        {
            msgType = "switchLane";
            data = direction;
        }
    }
}
