﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopernicusBot.Messages
{
    public class PingMsg : Msg
    {
        public PingMsg()
        {
            msgType = "ping";
        }
    }
}
