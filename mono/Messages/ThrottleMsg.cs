﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopernicusBot.Messages
{
    public class ThrottleMsg : Msg<double>
    {
        public ThrottleMsg(float value)
        {
            msgType = "throttle";
            this.data = value;
        }
    }
}
