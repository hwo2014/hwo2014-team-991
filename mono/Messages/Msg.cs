﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CopernicusBot.Messages
{
    public class Msg
    {
        public string msgType;

        [NonSerialized]
        private string _json = null;
        [NonSerialized]
        private string _jsonIndented = null;
        private EMsgDirection _msgDirection = EMsgDirection.Unknown;

        public string ToJson(bool indented, bool forceRefresh)
        {
            if (forceRefresh)
            {
                _jsonIndented = null;
                _json = null;
            }

            if (indented)
            {
                if (_jsonIndented == null)
                    _jsonIndented = JsonConvert.SerializeObject(this, Formatting.Indented);
                return _jsonIndented;
            }
            else
            {
                if (_json == null)
                    _json = JsonConvert.SerializeObject(this, Formatting.None);
                return _json;
            }
        }

        public enum EMsgDirection
        {
            Sent,
            Received,
            Unknown
        }

        public EMsgDirection MsgDirection()
        {
            return _msgDirection;
        }

        public void SetMsgDirection(EMsgDirection msgDirection)
        {
            _msgDirection = msgDirection;
        }
    }

    [Serializable]
    public class Msg<T> : Msg
    {
        public T data;
    }
}
