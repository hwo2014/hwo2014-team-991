﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CopernicusBot
{
    public class TrackPiece
    {
        public float length { get; set; }

        public bool @switch { get; set; }

        public bool bridge { get; set; }

        public float radius { get; set; }

        public float angle { get; set; }
    }
}
