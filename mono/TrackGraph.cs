﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuickGraph;
using QuickGraph.Algorithms;
using QuickGraph.Data;
using QuickGraph.Algorithms.ShortestPath;
using QuickGraph.Algorithms.Observers;
using QuickGraph.Graphviz;

namespace CopernicusBot
{
    public class TrackGraph
    {
        ArrayAdjacencyGraph<int, TaggedEdge<int, float>> m_graph;

        int numLanes;
        int finishVertex;

        public TrackGraph(Track track)
        {
            int currentVertex = 0;
            numLanes = track.lanes.Count;
            // numverts = 2*numlanes + numlanes * numswitch * 2 // track.pieces.Count()

            AdjacencyGraph<int, TaggedEdge<int, float>> graph = new AdjacencyGraph<int, TaggedEdge<int, float>>();

            // Vertices for Start
            for (int i = 0; i < numLanes; ++i)
            {
                graph.AddVertex(currentVertex);
                ++currentVertex;
            }

            foreach (TrackPiece piece in track.pieces)
            {
                for (int i = 0; i < numLanes; ++i)
                {
                    graph.AddVertex(currentVertex);
                    ++currentVertex;
                }

                for (int i = 0; i < numLanes; ++i)
                {
                    float trackSegmentLength = CalculateLength(piece, track.lanes[i].distanceFromCenter);
                    int target = currentVertex + i - numLanes;
                    int source = target - numLanes;
                    
                    graph.AddEdge(new TaggedEdge<int, float>(source, target, trackSegmentLength));
                }

                if (piece.@switch)
                {
                    // Criss-crosses through switch
                    int numSwitches = (numLanes - 1) * 2;
                    if (numSwitches > 0)
                    {
                        for (int i = 0; i < numSwitches - 1; ++i)
                        {
                            int switchEndA = currentVertex + i - numLanes;
                            int switchEndB = switchEndA + 1;

                            int switchStartA = switchEndB - numLanes;
                            int switchStartB = switchEndA - numLanes;

                            int laneA = switchStartA % numLanes;
                            int laneB = switchStartB % numLanes;

                            float length = CalculateXLength(piece, track.lanes[laneA].distanceFromCenter, track.lanes[laneB].distanceFromCenter);

                            graph.AddEdge(new TaggedEdge<int, float>(switchStartA, switchEndA, length));
                            graph.AddEdge(new TaggedEdge<int, float>(switchStartB, switchEndB, length));
                        }
                    }
                }    
            }

            // Target edge for path finding purposes
            graph.AddVertex(currentVertex);
            for (int i = 0; i < numLanes; ++i)
            {
                float length = 1.0f;
                graph.AddEdge(new TaggedEdge<int, float>(currentVertex - numLanes + i, currentVertex, length));
            }

            finishVertex = currentVertex;

            m_graph = graph.ToArrayAdjacencyGraph();
        }

        public int PickLaneSwitch(int pieceIndex, int laneIndex)
        {
            Func<TaggedEdge<int, float>, double> edgeCost = (edge => edge.Tag);

            int start = numLanes * pieceIndex + laneIndex;
            int end = finishVertex;

            var algo = new DijkstraShortestPathAlgorithm<int, TaggedEdge<int, float>>(m_graph, edgeCost);
            var predecessors = new VertexPredecessorRecorderObserver<int, TaggedEdge<int, float>>();
            using (predecessors.Attach(algo))
            {
                algo.Compute(start);
                IEnumerable<TaggedEdge<int, float>> path;

                if (predecessors.TryGetPath(end, out path))
                {
                    foreach(TaggedEdge<int, float> edge in path)
                    {
                        if (m_graph.OutEdges(edge.Source).Count() > 1)
                        {
                            int startLane = edge.Source % numLanes;
                            int nextLane = edge.Target % numLanes;
                            return nextLane - startLane;
                        }
                    }
                }

                return 0;
            }
        }

        public float CalculateLength(TrackPiece piece, float distanceFromCenter)
        {
            if (piece.length > 0.0f)
            {
                return piece.length;
            }

            if (piece.angle != 0.0f)
            {
                float radius = piece.radius;
                if (piece.angle > 0.0f)
                {
                    radius -= distanceFromCenter;
                }
                else
                {
                    radius += distanceFromCenter;
                }

                return (float)Math.Abs(piece.angle / 360.0f * 2.0f * (float)Math.PI * radius);
            }

            Console.WriteLine(string.Format("Bad Track Piece"));
            return 0.0f;
        }

        float CalculateXLength(TrackPiece piece, float distanceFromCenterLane1, float distanceFromCenterLane2)
        {
            float b = Math.Abs(distanceFromCenterLane1 - distanceFromCenterLane2);

            // Pythagorean theorem
            if (piece.length > 0.0f)
            {
                return (float)Math.Sqrt( piece.length * piece.length + b * b );
            }

            // Length of arc at the midpoint between the 2 lanes - Approximation for spiral arc length
            if (piece.angle != 0.0f)
            {
                float distFromCenter = Math.Min(distanceFromCenterLane1, distanceFromCenterLane2) + b * 0.5f;
                float radius = piece.radius;
                if (piece.angle > 0.0f)
                {
                    radius -= distFromCenter;
                }
                else
                {
                    radius += distFromCenter;
                }

                return (float)Math.Abs(piece.angle / 360.0f * 2.0f * (float)Math.PI * radius);
            }

            Console.WriteLine(string.Format("Bad Track Piece"));
            return 0.0f;
        }
    }
}
