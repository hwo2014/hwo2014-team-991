﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopernicusBot
{
    [Serializable]
    public class CarPositions
    {
        public CarPosition[] positions;
    }

    [Serializable]
    public class CarPosition
    {
        [Serializable]
        public class PiecePosition
        {
            [Serializable]
            public class Lane
            {
                public int startLaneIndex;
                public int endLaneIndex;
            }

            public int pieceIndex;
            public float inPieceDistance;
            public int lap;
            public Lane lane;
        }

        public CarId id;
        public float angle;
        public PiecePosition piecePosition;
    }
}
