﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CopernicusBot
{
    public class LapTime
    {
        public int lap { get; set; }
        public int ticks { get; set; }
        public int millis { get; set; }
    }
}
